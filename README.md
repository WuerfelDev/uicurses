# UICurses

A C++ curses UI framework to build your TUI apps with ease!

## todo

- impl. a window options structure to pass window-style, menus, buttons etc. to window
- new component UISelectbox
- make windows minimizable and maximizeable

## License

UICurses is licensed under the MIT license and requires you to acknowledge this in projects using the library.

[NCurses](https://invisible-island.net/ncurses/) is used in this library. It is licensed under MIT as well.
