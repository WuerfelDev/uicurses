#include <curses.h>
#include <iostream>
#include <algorithm>
#include <ncurses.h>
#include <string>

#include "UICurses.h"
#include "defines.h"

bool UICurses::startUI() {
    setlocale(LC_ALL, "");
    if (initscr()==NULL)
        return false;
    curs_set(0);
    clear();
    noecho();
    cbreak();
    refresh();
    return true;
}

void UICurses::stopUI() {
    curs_set(1);
    endwin();
}

bool UICurses::Component::clicked(const MEVENT* const mouseEvent) {
	if (mouseEvent->bstate & BUTTON1_CLICKED)
        return checkDim(mouseEvent, dimension);
	return false;
}

bool UICurses::Component::pressed(const MEVENT* const mouseEvent) {
	if (mouseEvent->bstate & BUTTON1_PRESSED)
        return checkDim(mouseEvent, dimension);
	return false;
}

bool UICurses::Component::released(const MEVENT* const mouseEvent) {
	if (mouseEvent->bstate & BUTTON1_RELEASED)
        return true;
	return false;
}

bool UICurses::Component::checkDim(const MEVENT* const mouseEvent, const Dimension& dim){
    if (mouseEvent->y >= dim.y &&
			mouseEvent->y <= dim.y + dim.h &&
			mouseEvent->x >= dim.x &&
			mouseEvent->x <= dim.x + dim.w
        )
            return true;
    return false;
}
void UICurses::Component::setText(std::string text) {
    // Adjust box dim if necessary
    this->text = std::string(" " + text + " ");
    dimension.h = std::max(dimension.h, (ushort)1); 
    dimension.w = std::max(dimension.w, (ushort)this->text.length());
}


UICurses::Window::Window(std::string title, Dimension dimension, int wb)
: Component(title, dimension) {

    /* setting title */
    if (!(title.empty())) {
        this->title = new Textfield(title, {.x=TITLE_PADDING, .y=0});
        addComponent(this->title);
    }

    /* calc min/max dim */
    this->dimension.w = std::max(this->dimension.w, MIN_WIN_WIDTH);
    this->dimension.h = std::max(this->dimension.h, (ushort)2);

    if (0 < (this->dimension.x+this->dimension.w - COLS) 
        && (this->dimension.x+this->dimension.w - COLS) < this->dimension.x)
        this->dimension.x -= (this->dimension.x+this->dimension.w - COLS);
    else
        this->dimension.w = std::min(this->dimension.w, (ushort)(COLS-this->dimension.x));
    
    if (0 < (this->dimension.y+this->dimension.h - LINES) 
        && (this->dimension.y+this->dimension.h - LINES) < this->dimension.y)
        this->dimension.y -= (this->dimension.y+this->dimension.h - LINES);
    else
        this->dimension.h = std::min(this->dimension.h, (ushort)(LINES-this->dimension.y));

    /* set mouse events */
    keypad(stdscr, true);
    mousemask(BUTTON1_PRESSED | BUTTON1_RELEASED | BUTTON1_CLICKED | BUTTON1_DOUBLE_CLICKED | REPORT_MOUSE_POSITION, nullptr);

    /* show window buttons */
    if (wb & OK_BTN)
        addComponent(prototype(OK_BTN));

    if (wb & CANCEL_BTN)
        addComponent(prototype(CANCEL_BTN));

    if (wb & EXIT_BTN) {
        std::function<bool()> lambda = [this]() { this->isActive = false; return false; };
        Button* bExit = new Button("X", {.x=(ushort)(this->dimension.w-X_BTN_PADDING), .y=0,}, lambda);
        addComponent(bExit);
    }
}

UICurses::Window::~Window() {
    for (auto component : components) {
        delete component;
    }
}

bool UICurses::Window::show() {
    bool result;
    bool titlePressed = false;
    ushort diff;

     /* generate box and panel */
    hWindow = newwin(dimension.h, dimension.w, dimension.y, dimension.x);
    box(hWindow, 0, 0);
    hPanel = new_panel(hWindow);

    /* show components first time */
    for (auto component : components)
        component->show();
    wrefresh(hWindow);

    /* window loop */
    while (isActive && getch()) {
        getmouse(&mouseEvent);
        relativeMousePosition(&mouseEvent);

        /* move window */
        if (title->pressed(&mouseEvent)) {
            diff = mouseEvent.x;
            titlePressed = true;
        }
        else if (this->released(&mouseEvent) && titlePressed) {
            move(&mouseEvent, diff);
            titlePressed = false;
        }

        /* execute component callbacks & redraw components */
        for (auto component : components) {
            if (component->clicked(&mouseEvent) && component->callback)
                result = component->callback();
        }
        for (auto component : components)
            component->show();

        wrefresh(hWindow);
        update_panels();
        doupdate();
    }

    del_panel(hPanel);
    wclear(hWindow);
    delwin(hWindow);

    return result;
}

// bool UICurses::Window::okBtn() {
//     isActive = false;
//     return true;
// }

// bool UICurses::Window::cancelBtn() {
//     isActive = false;
//     return false;
// }

void UICurses::Window::move(MEVENT* mouseEvent, ushort diff) {
    // Keep x value within screen bounds
    if (diff > dimension.x+mouseEvent->x)
        dimension.x = 0;
    else if (dimension.x+mouseEvent->x-diff >= COLS-dimension.w)
        dimension.x = COLS-dimension.w;
    else
        dimension.x += (ushort)mouseEvent->x-diff;

    // Keep y value within screen bounds
    if (dimension.y+mouseEvent->y+dimension.h > LINES)
        dimension.y = LINES - dimension.h;
    else
        dimension.y += (ushort)mouseEvent->y;
    move_panel(hPanel, dimension.y, dimension.x);
}

void UICurses::Window::debug(ushort row, std::string output) {
    mvaddstr(row, 0, "                        ");
    mvaddstr(row, 0, output.c_str());
    refresh();
}

void UICurses::Window::debug(ushort row, int output) {
    mvaddstr(row, 0, "                        ");
    mvaddstr(row, 0, std::to_string(output).c_str());
    refresh();
}

void UICurses::Window::setText(std::string title) {
    if (this->title)
        this->title->setText(title);
}

void UICurses::Window::addComponent(Component* component) {
    if (!component)
        return;
        
    checkOverlapping(component);
    component->setHandle(this);
    components.push_back(component);
}

void UICurses::Window::checkOverlapping(Component* component) {
    bool overlapping;

    do {
        overlapping = false;

        for (auto c : components) {
            if (/* x-dimension */
                ((component->dimension.x >= c->dimension.x
                && component->dimension.x <= (c->dimension.x+c->dimension.w))
                || (c->dimension.x >= component->dimension.x
                && c->dimension.x <= (component->dimension.x+component->dimension.w)))
                /* y-dimension */
                && ((component->dimension.y >= c->dimension.y
                && component->dimension.y < (c->dimension.y+c->dimension.h))
                || (c->dimension.y >= component->dimension.y
                && c->dimension.y < (component->dimension.y+component->dimension.h)))
                ) {
                    component->dimension.y++;
                    overlapping = true;
                }
        }
    }
    while (overlapping);
}

void UICurses::Window::relativeMousePosition(MEVENT* mouseEvent)
{
	mouseEvent->x -= dimension.x;
	mouseEvent->y -= dimension.y;
}

UICurses::Component* UICurses::Window::prototype(WindowComponents wc) {
    switch (wc) {
        case OK_BTN:
            {
                std::function<bool()> lambda = [this]() { this->isActive = false; return true; };
                Button* bOk = new Button("OK", {.x=LEFT_BTN_PADDING, .y=(ushort)(this->dimension.h-HEIGHT_BTN_PADDING)}, lambda);
                return bOk;
            }
        case CANCEL_BTN:
            {
                std::function<bool()> lambda = [this]() { this->isActive = false; return false; };
                Button* bCancel = new Button("Cancel", {.x=(ushort)(this->dimension.w-RIGHT_BTN_PADDING),.y=(ushort)(this->dimension.h-HEIGHT_BTN_PADDING)}, lambda);
                return bCancel;
            }
        default:
            return nullptr;
    }
}


////////////////// COMPONENTS /////////////////

bool UICurses::Button::show() {
    wattron(hParent->getHandle(), A_REVERSE);
	mvwaddstr(hParent->getHandle(), dimension.y, dimension.x, text.c_str());
    wattroff(hParent->getHandle(), A_REVERSE);

    return true;
}

bool UICurses::Textfield::show() {
    mvwaddstr(hParent->getHandle(), dimension.y, dimension.x, text.c_str());
    
    return true;
}

UICurses::Menu::Menu(Component* sender)
: Window("", {.x=(ushort)(sender->getHandle()->getDimension().x+sender->getDimension().x),
                .y=(ushort)(sender->getHandle()->getDimension().y+2),
                .h=2}, EXIT_BTN) {}

void UICurses::Menu::addComponent(Component* component) {
    if (!component)
        return;
    
    dimension.h += component->getDimension().h;
    component->setDimension({.x=2, .y=1, .w=component->getDimension().w, .h=component->getDimension().h});

    Window::addComponent(component);
}

UICurses::Menubar::Menubar(Window* hParent)
: Component("", {.x=(ushort)(hParent->getDimension().x+1),
                .y=(ushort)(hParent->getDimension().y+2),
                .h=1}) {}

void UICurses::Menubar::alignComponent(Component* component) {
    if (!component)
        return;
        
    component->setDimension({.x=nextPos, .y=1, .w=component->getDimension().w, .h=component->getDimension().h});
    nextPos += (component->getDimension().w + 2);
}

bool UICurses::Seperator::show() {
    for (ushort i=0; i<hParent->getDimension().w-4; i++)
        mvwaddch(hParent->getHandle(), dimension.y, dimension.x+i, ACS_HLINE);
        
    return true;
}