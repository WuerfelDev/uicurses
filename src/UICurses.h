#ifndef UICURSES_H
#define UICURSES_H

#include <string>
#include <vector>
#include <functional>
#include <ncurses.h>
#include <panel.h>

#include "defines.h"

namespace UICurses {

bool startUI();
void stopUI();

/* forward-declarations */
class Window;
class Textfield;
class Menubar;

class Component {

	protected:
		Window* hParent = nullptr;
		std::string text;
		Dimension dimension;
		std::function<bool()> callback;

		bool clicked(const MEVENT* const mouseEvent);
		bool pressed(const MEVENT* const mouseEvent);
		bool released(const MEVENT* const mouseEvent);

		bool checkDim(const MEVENT* const mouseEvent, const Dimension& dim);
		
	public:
		Component(std::string text = std::string(), Dimension dimension = {}, std::function<bool()> callback=nullptr)
		: text(text), dimension(dimension), callback(callback) { setText(text); }
		virtual ~Component() {};

		void setHandle(Window* handle ) { hParent = handle; }
		Window* getHandle() const { return hParent; }
		void setDimension(Dimension dimension) { this->dimension = dimension; }
		Dimension getDimension() const { return dimension; }

		virtual void setText(std::string);
		virtual bool show() = 0;

		friend class Window;
};

class Window : public Component {

	protected:
		WINDOW* hWindow = nullptr;
		PANEL* hPanel = nullptr;
		Textfield* title;
		MEVENT mouseEvent;
		bool isActive = true;

		std::vector<Component*> components;

		void checkOverlapping(Component* component);
		void move(MEVENT* mouseEvent, ushort diff);

		void debug(ushort row, std::string output);
		void debug(ushort row, int output);

	public:
		Window(std::string title = std::string(), Dimension dimension = {}, int wc = NONE_BTN);
		~Window();

		WINDOW* getHandle() const { return hWindow; }
		void setText(std::string text) override;
		bool show() override;
		void close() { isActive = false; }

		ushort getMidV() const { return (dimension.w/2 + dimension.x); }
		ushort getMidH() const { return (dimension.h/2 + dimension.y); }
		virtual void addComponent(Component* component);
		void relativeMousePosition(MEVENT* mouseEvent);
		Component* prototype(WindowComponents wc);
};

class Button : public Component {

	public:
		Button(std::string text, Dimension dimension, std::function<bool()> callback)
		: Component(text, dimension, callback) {}

		bool show() override;
};

class Textfield : public Component {

	public:
		Textfield(std::string text, Dimension dimension)
		: Component(text, dimension) {}

		bool show() override;
};

class Menubar : public Component {

	protected:
		ushort nextPos = 2;

	public:
		Menubar(Window* hParent);

		void alignComponent(Component* component);
		bool show() override { return true; }
};

class Menu : public Window {

	public:
		Menu(Component* sender);

		void addComponent(Component* component) override;
};

class Seperator : public Component {	

	public:
		Seperator() : Component() {}

		bool show() override;
};

}

#endif // !UICURSES_H
