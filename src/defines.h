#ifndef DEFINES_H
#define DEFINES_H

// typedef __attribute__((__ext_vector_type__(2))) unsigned short Dimension;
typedef unsigned short ushort;

namespace UICurses {

enum WindowComponents {
	NONE_BTN = 0x0,
	OK_BTN = 0x1,
	CANCEL_BTN = 0x2,
	EXIT_BTN = 0x4,
	TEXTFIELD = 0x8,
};

struct Dimension {
	ushort x,y,w,h;
};

#define TITLE_PADDING (ushort)4
#define MIN_WIN_WIDTH (ushort)14
#define HEIGHT_BTN_PADDING (ushort)3
#define LEFT_BTN_PADDING (ushort)6
#define RIGHT_BTN_PADDING (ushort)(8+6)
#define X_BTN_PADDING (ushort)3

}


#endif // !DEFINES_H