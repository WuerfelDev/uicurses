#include <cstdlib>
#include <iostream>
#include <signal.h>

#include "UICurses.h"

using namespace UICurses;

void interupt_handler(sig_atomic_t s)
{
    UICurses::stopUI();
    exit(EXIT_FAILURE);
}

UICurses::Window* w;
UICurses::Window* w2;
UICurses::Window* aboutWin;
UICurses::Button* bFile;
UICurses::Button* bAbout;
UICurses::Button* bWhat;
UICurses::Button* bTest;
UICurses::Button* bExit;
UICurses::Textfield* tf;
UICurses::Textfield* tf2;
UICurses::Seperator* sep;
UICurses::Menu* m;

bool changeBtnCallback() {
    w2->setText("changed title");

    return true;
}

bool aboutBtnCallback() {
    aboutWin = new UICurses::Window("About", {.x=(ushort)(w->getMidV()-15),
                                        .y=(ushort)(w->getMidH()-7),
                                        .w=30,.h=15}, UICurses::OK_BTN);
    aboutWin->show();
    delete aboutWin;

    m->close();

    return true;
}

bool testBtnCallback() {
    w2 = new UICurses::Window("2nd window", {.x=(ushort)(w->getMidV()-15),
                                            .y=(ushort)(w->getMidH()-7),.w=30,.h=15}, 
                                            UICurses::OK_BTN | UICurses::EXIT_BTN);
    UICurses::Button* bCancel = reinterpret_cast<UICurses::Button*>(w2->prototype(UICurses::CANCEL_BTN));
    w2->addComponent(bCancel);

    Button* bChangeTitle = new Button("change title", {.x=1,.y=1}, changeBtnCallback);
    w2->addComponent(bChangeTitle);

    if (w2->show())
        w->setText("New main window title");
    delete w2;

    m->close();

    return true;
}

bool exitBtnCallback() {
    UICurses::stopUI();
    exit(EXIT_SUCCESS);

    return true;
}

bool fileBtnCallback() {
    m = new Menu(bFile);

    bTest = new UICurses::Button("Test", {}, testBtnCallback);
    m->addComponent(bTest);

    sep = new UICurses::Seperator();
    m->addComponent(sep);

    bExit = new UICurses::Button("Exit", {}, exitBtnCallback);
    m->addComponent(bExit);

    m->show();
    delete m;

    return true;
}

bool whatBtnCallback() {
    m = new Menu(bWhat);

    bAbout = new UICurses::Button("About", {}, aboutBtnCallback);
    m->addComponent(bAbout);

    m->show();
    delete m;

    return true;
}

int main () {

    if(!UICurses::startUI()){
        std::cout << "Problem starting ncurses" << std::endl;
        exit(EXIT_FAILURE);
    }

    // Set signal to catch program closure, and exit out of ncurses
    signal(SIGINT, interupt_handler);
    signal(SIGTERM, interupt_handler);

    w = new UICurses::Window("Main window", {.x=8,.y=3,.w=120,.h=30}, UICurses::EXIT_BTN);

    tf = new UICurses::Textfield("xtest", {.x=14, .y=5});
    tf2 = new UICurses::Textfield("xtest", {.x=18, .y=5});
    w->addComponent(tf);
    w->addComponent(tf2);


    bFile = new Button("File", {}, fileBtnCallback);
    bWhat = new Button("?", {}, whatBtnCallback);
    w->addComponent(bFile);
    w->addComponent(bWhat);

    Menubar* mb = new Menubar(w);
    mb->alignComponent(bFile);
    mb->alignComponent(bWhat);
    w->addComponent(mb);

    w->show();
    delete w;

    UICurses::stopUI();

    return EXIT_SUCCESS;
}
